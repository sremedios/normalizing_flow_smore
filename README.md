# normalizing_flow_smore

Normalizing flow is a distribution estimation technique using invertible neural networks. We will use it here to 1. detect whether a volume's through-plane distribution matches its in-plane distribution and 2. improve SMORE.