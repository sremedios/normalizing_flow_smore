import tensorflow as tf
import tensorflow_probability as tfp
import numpy as np

tfb = tfp.bijectors
tfd = tfp.distributions
tfk = tf.keras
tfkl = tf.keras.layers
tfpl = tfp.layers

class Coupling(tfb.Bijector):
    def __init__(
        self, 
        shift_and_log_scale_fn,
        mask_fn,
        validate_args=False, 
        name='coupling',
    ):

        self.shift_and_log_scale_fn = shift_and_log_scale_fn
        self.mask_fn = mask_fn
        
        # This internal function will set up a shift_scale_bijector
        # after running `a` through the internal `shift_and_log_scale_fn`
        def _create_shift_scale_bijector(a):
            shift, log_scale = self.shift_and_log_scale_fn(a)
            scale = tf.exp(log_scale)
            shift_scale_bijector = tfb.Shift(shift=shift)(tfb.Scale(scale=scale))
            return shift_scale_bijector
        
        self.create_shift_scale_bijector = _create_shift_scale_bijector
    
        super(Coupling, self).__init__(
            forward_min_event_ndims=1,
            is_constant_jacobian=False,
            validate_args=validate_args,
            name=name,
        )
        
    def _forward(self, x):
        x_id, x_change = self.mask_fn.mask(x)

        # input to the network is `x_id`
        shift_scale_bijector = self.create_shift_scale_bijector(x_id)
        # apply transform to `x_change`
        y_change = shift_scale_bijector.forward(x_change)

        y = self.mask_fn.unmask(x_id, y_change)

        return y

    
    def _inverse(self, y):
        y_id, y_change = self.mask_fn.mask(y)

        # input to network is `y_id`
        shift_scale_bijector = self.create_shift_scale_bijector(y_id)
        # apply inverse transform to `y_change`
        x_change = shift_scale_bijector.inverse(y_change)

        x = self.mask_fn.unmask(y_id, x_change)
        
        return x
    
    def _forward_log_det_jacobian(self, x):
        x_id, x_change = self.mask_fn.mask(x)
        
        # input to the network is `x_id`
        shift_scale_bijector = self.create_shift_scale_bijector(x_id)
        
        # return log det jacobian on `x_change`
        return shift_scale_bijector.forward_log_det_jacobian(x_change, event_ndims=1)
        
        
    
    def _inverse_log_det_jacobian(self, y):
        y_id, y_change = self.mask_fn.mask(y)

        # input to network is `y_id`
        shift_scale_bijector = self.create_shift_scale_bijector(y_id)
        
        # return log det jacobian on `y_change`
        return shift_scale_bijector.inverse_log_det_jacobian(y_change, event_ndims=1)
        
    
    