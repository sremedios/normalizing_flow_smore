import tensorflow as tf
import contextlib

def forward(inputs, model, loss_fn, training=False):
    xs, ys = inputs

    if training:
        context = tf.GradientTape()
    else:
        context = contextlib.nullcontext()

    with context as tape:
        logits = model(xs, training=training)
        loss = tf.reduce_mean(loss_fn(ys, logits))

    if training:
        grad = tape.gradient(loss, model.trainable_variables)
        return grad, loss, logits

    return loss, logits

def forward_with_aux(inputs, model, loss_fn, training=False):
    xs, auxs, ys = inputs

    if training:
        context = tf.GradientTape()
    else:
        context = contextlib.nullcontext()

    with context as tape:
        logits, aux_logits = model(xs, training=training)
        loss = tf.reduce_mean(loss_fn(ys, logits)) + tf.reduce_mean(loss_fn(auxs, aux_logits))

    if training:
        grad = tape.gradient(loss, model.trainable_variables)
        return grad, loss, logits

    return loss, logits
