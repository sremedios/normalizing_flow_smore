import numpy as np
from skimage.util.shape import view_as_windows

def get_view_indices(img, patch_size):
    views = view_as_windows(img, patch_size)
    indices = np.array(list(np.ndindex(views.shape[:len(patch_size)])))
    return indices

def enumerate_views(img_lr, img_hr, patch_size):
    '''
    Returns indices and all views of size `patch_size` from `img`.
    Warning: Executing any mutable operation on the return
    of this function will cause severe memory usage.
    '''
    
    assert img_lr.shape == img_hr.shape
    
    views_lr = view_as_windows(img_lr, patch_size)
    views_hr = view_as_windows(img_hr, patch_size)

    indices = np.array(list(np.ndindex(views_lr.shape[:len(patch_size)])))

    return indices, (views_lr, views_hr)