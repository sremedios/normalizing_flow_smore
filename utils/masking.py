import tensorflow as tf
import numpy as np

'''
Code adapted from https://github.com/PolinaKirichenko/flows_ood
'''

class MaskChannelwise():
    def __init__(self, flip):
        self.flip = flip
    
    def mask(self, x):
        x_id, x_change = tf.split(x, 2, axis=-1)
        
        if self.flip:
            x_id, x_change = x_change, x_id
        return x_id, x_change
    
    def unmask(self, x_id, x_change):
        if self.flip:
            return tf.concat([x_id, x_change], axis=-1)
        else:
            return tf.concat([x_change, x_id], axis=-1)
    
class MaskCheckerboard:
    def __init__(self, flip):
        self.flip = flip

    def mask(self, x):
        
        self.checkerboard = tf.Variable((np.indices(x.shape).sum(axis=0) % 2).astype(np.float32))
        
        if self.flip:
            self.checkerboard = 1 - self.checkerboard

        x_id = x * self.checkerboard
        x_change = x * (1 - self.checkerboard)
        return x_id, x_change

    def unmask(self, x_id, x_change):
        return x_change * self.checkerboard + x_id * (1 - self.checkerboard)