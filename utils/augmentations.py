import numpy as np
from scipy.ndimage import rotate

def random_rotate(x, y, mode='mirror'):
    angle = np.random.choice(range(-60,60))
    x = rotate(x, angle, reshape=False, mode=mode)
    y = rotate(y, angle, reshape=False, mode=mode)
    return x, y