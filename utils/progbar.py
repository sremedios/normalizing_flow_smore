import sys

def show_progbar(
        cur_step, 
        total_steps, 
        batch_size,
        num_instances, 
        loss,
        color_code,
        elapsed_time,
        progbar_length=10,
        ):

    # first clear last line
    ERASE_LINE = '\x1b[2K'
    sys.stdout.write(ERASE_LINE)

    # write template
    TEMPLATE = (
        "\r{}Step {:3d}/{} [{:{}<{}}] "
        "Loss: {:>4.4f} " 
        " |  "
        "{:.2f}s / it | "
        "ETA: {:.1f}s | "
        "Total: {:.1f}s"
        "\033[0;0m"
       )

    # calculate eta
    num_seen = num_instances - (cur_step * batch_size) 
    eta = num_seen / batch_size * elapsed_time
    if eta < 0:
        eta = 0

    # calculate total
    total = min(num_instances, cur_step * batch_size) / batch_size * elapsed_time

    current_progress = "=" * min(
            int(progbar_length*((cur_step*batch_size)/num_instances)), 
            progbar_length,
        )

    # write
    sys.stdout.write(TEMPLATE.format(
        color_code,
        cur_step, 
        total_steps,
        current_progress,
        "-",
        progbar_length,
        loss,
        elapsed_time,
        eta,
        total,
    ))
    sys.stdout.flush()
