import numpy as np


def fba(imgs):
    final_fft_data, final_fft_mag = None, None

    for img in imgs:
        fft_data = np.fft.rfftn(img)
        fft_mag = np.absolute(fft_data)
        if final_fft_data is None:
            final_fft_data = fft_data
            final_fft_mag = fft_mag
        mask_update = np.less(final_fft_mag, fft_mag)
        final_fft_mag[mask_update] = fft_mag[mask_update]
        final_fft_data[mask_update] = fft_data[mask_update]

    final_data = np.absolute(np.fft.irfftn(final_fft_data))
    return final_data
